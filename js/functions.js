/*
	Libreria de funciones creadas
	para el procesamiento del Inicio de Sesion
	Modificado por: Ing. Gustavo Aparicio
	Modificada: 12/08/2019
*/
function getOrderChar(param)
{
	var num = 0;
	var len_1 = param.length;
	var c1 = ' ';
	var c2 = ' ';
	var c3 = 'X';
	
	for(var i = 0; i < len_1; i++)
	{
		num = Math.round(Math.random() * (10 - 1) + 1);
		
		if((len_1 > (i+num)) && num !=0 && (i+num)>0 ){
                            
			c1=param.charAt(i);
			c2=param.charAt(i+num);
			param = param.replace(c2,c3);
			param = param.replace(c1,c2);
			param = param.replace(c3,c1);
		}				
	}
	return param;

};

function replaceChart(index,c, cad){
        var cadena="";
        
        if(cad.length>0){
            cadena=cad.substr(0,index)+c+cad.substr(index+1);
        }
        
        return cadena;
}

function cifrar(dato,llave1,llave2){

    var x='';
    var y='';
    var index=0;
    dato = dato.toUpperCase();
    var i=0;
    
    for(i=0;i < dato.length;i++){
        x=dato.charAt(i);
        index=llave1.indexOf(x);
        if(index > -1){
            y=llave2.charAt(index);
                dato=replaceChart(i,y,dato);
        }else{
            index=llave2.indexOf(x);
            if(index > -1){
                y=llave1.charAt(index);
                dato=replaceChart(i,y,dato);
            }   
        }
        
    }

	return dato;
};

$(document).ready(function(){

	var llave1="6183425790QWERTYUIOPASDFGHJKLZXCVBNM";
	var llave2="|h!#v$%r&(m)x?f+{y},-_:;[w]*~.k<s>@q";
	var num = 0;
	var len_1 = llave1.length;
	var len_2 = llave2.length;
	var c1 = ' ';
	var c2 = ' ';
	var c3 = 'X';	
	var llave3 = getOrderChar(llave1);
	var llave4 = getOrderChar(llave2);
 	$('#llave1').val(llave3);
 	$('#llave2').val(llave4);
 	
	$('#ingresar').on('click',function(e){
	
		e.preventDefault();
		
		var user = $('#UserId').val();
		var pwd = $('#UserPass').val();
		var llave1 = $('#llave1').val();
		var llave2 = $('#llave2').val();
		var userf = "";
		var pwdf = "";
	
		if(user == ""){
			alert("Favor de Ingresar Usuario.");
			return false;
		}
	
		if(pwd == ""){
			alert("Favor de Ingresar su Clave.");
			return false;
		}
	
		userf = cifrar(user,llave1,llave2);
		pwdf = cifrar(pwd,llave1,llave2);
		
		$('#UserId').val(userf);
		$('#UserPass').val(pwdf);
		
		$('#form_login').submit();	
		$('#loader').fadeIn('slow');
	
	}); 	
	
	$('#UserPass').on('paste',function(e){
		e.preventDefault();
	});

	$('#UserPass').on('copy',function(e){
		e.preventDefault();
	});

	$('#UserId').on('paste',function(e){
		e.preventDefault();
	});

	$('#UserId').on('copy',function(e){
		e.preventDefault();
	});
	
	$('#UserPass').on('keypress', function(e){
		let teclaPulsada = e.charCode;
		
		if((teclaPulsada >= 48 && teclaPulsada <= 57) || (teclaPulsada >= 65 && teclaPulsada <= 90) || (teclaPulsada >= 97 && teclaPulsada <= 122))
		{
			return true;
		}
		
		return false;
	});

	var msgU = "Solicitud inv�lida";
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
        alert(msgU);
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
    	alert(msgU);
        return false;
    });
 	
});
$(window).ready(function() {
    $(".loader").delay(600).fadeOut("slow");
});