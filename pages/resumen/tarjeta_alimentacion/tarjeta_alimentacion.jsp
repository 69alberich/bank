<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Tarjeta de alimentación</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/tarjeta_alimentacion.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="heading">
                <h3>CONSULTA</h3>
            </div>
            <table class="information">
                <thead>
                    <th class="padding">Tarjeta</th>
                    <th class="center">SALDO DISPONIBLE</th>

                </thead>
                <tbody>
                    <tr>
                        <td class="padding">
                            <p id="tarjeta"></p>
                        </td>
                        <td class="padding">
                            <p class="center" id="saldo"></p>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="suboption" id="horizontal_menu">
                <a href="http://localhost:3000/pages/resumen/tarjeta_alimentacion/tarjeta_alimentacion.jsp" class="red"><b>CONSULTA DEL MES</b></a>
                <a href="http://localhost:3000/pages/resumen/tarjeta_alimentacion/transferencia_fondos.jsp" class=""><b>TRANSFERENCIA DE FONDOS</b></a>
            </div>

            <div class="selector">
                <label>Mes</label>
                <select></select>
            </div>

            <div>
                <div class="structure">
                    <div class="container">
                        <div>
                            <p>FECHA</p>
                        </div>
                        <div>
                            <p>DESCRIPCIÓN</p>
                        </div>
                        <div>
                            <p>REFERENCIA</p>
                        </div>
                        <div>
                            <p>DÉBITO</p>
                        </div>
                        <div>
                            <p>CRÉDITO</p>
                        </div>
                        <div>
                            <p>SALDO</p>
                        </div>
                    </div>
                    <table class="table-mv">
                        <tbody id="info" class="tbody">
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        $('.creditos').load('http://localhost:3000/components/fideicomiso.html');
    });
    const tarjeta = document.getElementById('tarjeta');
    const info = document.getElementById('info');
    const saldo = document.getElementById('saldo');

    fetch("http://localhost:3000/data/tarjeta_alimentacion.json")
        .then(response => {
            return response.json();
        })
        .then(data => {
            tarjeta.innerHTML = data.info.tarjeta
            saldo.innerHTML = `
                <p>Bs ${data.info.saldo.bs}</p>
                <p>PTR ${data.info.saldo.ptr}</p>
            `
            
            data.info.movimientos.map((mov) => {
                info.innerHTML += `
                <tr>
                    <td>
                        <p>${mov.fecha}</p>
                    </td>
                    <td>
                        <p>${mov.descripcion}</p>
                    </td>
                    <td>
                        <p>${mov.referencia}</p>
                    </td>
                    <td>
                        <p>Bs ${mov.debito}</p>
                    </td>
                    <td>
                        <p>Bs ${mov.debito}</p>
                    </td>
                    <td>
                        <p>Bs ${mov.saldo}</p>
                    </td>
                </tr>
                `
            })
        })
</script>

</html>