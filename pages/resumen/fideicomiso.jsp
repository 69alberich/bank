<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Fideicomiso</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/sidebar.css">
    <link rel="stylesheet" href="http://localhost:3000/css/diferidos_bloqueados.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="heading">
                <h3>CONSULTA</h3>
            </div>
            <table class="information">
                <thead>
                    <th class="padding">Contrato</th>
                </thead>
                <tbody>
                    <tr>
                        <td class="padding">
                            <p id="contract"></p>
                        </td>
                    </tr>
                </tbody>
                <thead>
                    <th class="padding">Nombre</th>
                    <th class="center">Nombre del patrono</th>
                </thead>
                <tbody>
                    <tr>
                        <td class="padding">
                            <p id="name"></p>
                        </td>
                        <td class="padding">
                            <p class="center" id="patrono">patrono</p>
                        </td>
                    </tr>
                </tbody>
                <thead>
                    <th class="padding">Cédula de identidad</th>
                    <th class="center">Rif del patrono</th>
                </thead>
                <tbody>
                    <tr>
                        <td class="padding">
                            <p id="cedula"></p>
                        </td>
                        <td class="padding">
                            <p class="center" id="rif_patrono"></p>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div>
            <p class="title">Saldo del Beneficiario</p>

                <div class="structure">
                    <div class="container">
                        <div>
                            <p>CAPITAL</p>
                        </div>
                        <div>
                            <p>EMBARGOS / BLOQUEOS</p>
                        </div>
                        <div>
                            <p>ANTICIPOS</p>
                        </div>
                        <div>
                            <p>CAPITAL NETO</p>
                        </div>
                        <div>
                            <p>DISPONIBLE RETIRO</p>
                        </div>
                    </div>
                    <table class="table-mv">
                        <tbody id="info" class="tbody">
                    
                        </tbody>
                    </table>
                    <p class="info-p"><b>Los saldos reflejados en la consulta son de carácter referencial y están sujetos a revisión por parte de la institución.</b></p>
                </div>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        $('.creditos').load('http://localhost:3000/components/fideicomiso.html');
    });
    const contract = document.getElementById('contract');
    const name = document.getElementById('name');
    const patrono = document.getElementById('patrono');
    const cedula = document.getElementById('cedula');
    const rif_patrono = document.getElementById('rif_patrono');
    const info = document.getElementById('info');

    fetch("http://localhost:3000/data/fideicomiso.json")
        .then(response => {
            return response.json();
        })
        .then(data => {
            contract.innerHTML = data.fideicomiso.contrato
            name.innerHTML = data.fideicomiso.name
            patrono.innerHTML = data.fideicomiso.patrono_name
            cedula.innerHTML = data.fideicomiso.cedula
            rif_patrono.innerHTML = data.fideicomiso.rif_patrono

            info.innerHTML += `
                <tr>
                    <td>
                        <p>Bs ${data.fideicomiso.capital.bs}</p>
                    </td>
                    <td>
                        <p>Bs ${data.fideicomiso.embargo.bs}</p>
                    </td>
                    <td>
                        <p>Bs ${data.fideicomiso.anticipo.bs}</p>
                    </td>
                    <td>
                        <p>Bs ${data.fideicomiso.capital_neto.bs}</p>
                    </td>
                    <td>
                        <p>Bs ${data.fideicomiso.disponible.bs}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>PTR ${data.fideicomiso.capital.ptr}</p>
                    </td>
                    <td>
                        <p>PTR ${data.fideicomiso.embargo.ptr}</p>
                    </td>
                    <td>
                        <p>PTR ${data.fideicomiso.anticipo.ptr}</p>
                    </td>
                    <td>
                        <p>PTR ${data.fideicomiso.capital_neto.ptr}</p>
                    </td>
                    <td>
                        <p>PTR ${data.fideicomiso.disponible.ptr}</p>
                    </td>
                </tr>
                `
        })
</script>

</html>