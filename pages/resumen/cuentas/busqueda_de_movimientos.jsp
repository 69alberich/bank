<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Busqueda de movimientos</title>
    
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/sidebar.css">
    <link rel="stylesheet" href="http://localhost:3000/css/movimientos.css">
    <link rel="stylesheet" href="http://localhost:3000/css/busqueda_movimientos.css">
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="heading">
                <h3>RESUMEN GLOBAL</h3>
            </div>
            <table class="information">
                <thead>
                    <th class="red padding">CUENTAS</th>
                    <th class="center">SALDO DISPONIBLE</th>
                </thead>
                <tbody>
                    <tr>
                        <td class="padding">
                            <p><b>Ahorro</b></p>
                            <p>xxxx 5555 xxxx 5555</p>
                        </td>
                        <td class="padding">
                            <p class="center">Bs 55.555.555</p>
                            <p class="center">Petro 55.555.55</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="sidebar"></div>
            
            <form class="search">
                <div class="input-group">
                    <label>Desde</label>
                    <input class="input" type="date"/>
                </div>
                <div class="input-group">
                    <label>Hasta</label>
                    <input class="input" type="date"/>
                </div>
                <div class="select-group">
                    <label>Tipo de operación</label>
                    <select>
                        <option></option>
                    </select>
                </div>
                <div class="input-group top">
                    <label>Montos</label>
                    <input class="input" type="number" placeholder="Desde"/>
                </div>
                <div class="input-group top">
                    <br/>
                    <input class="input" type="number" placeholder="Hasta"/>
                </div>
            </form>

            <div class="movimientos"></div>
        </div>
    </div>

    <footer class="footer"></footer>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.sidebar').load('http://localhost:3000/components/sidebar.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        $('.movimientos').load('http://localhost:3000/components/movimientos.html');
    });
</script>
</html>