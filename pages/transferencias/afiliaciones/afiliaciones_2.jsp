<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Afiliaciones</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            
            <div class="container ">
                <div class="row">
                <div class="heading mb-2">
                    <h3>AFILIACIONES</h3>
                    
                </div>
                <p class="text-justify py-3">Le recordamos que hemos incorporado a nuestras políticas de seguridad
                    del Código de Validación, el cual recibirá a través de su teléfono celular y correo
                    electrónico
                </p>
                
                    <div class="heading-blue mb-2">
                        <h3>VALIDACIÓN CON TARJETA CONFIDENCIAL</h3>
                        
                    </div>
                </div>
                
            </div>
            
            
                
           <div class="container">
               <form action="" class="custom-form">
                <ol class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                      <div class="ms-2 me-auto">
                       
                        Verifique que el siguiente serial sea el de su tarjeta confidencial: 55555555
                      </div>
                      
                    </li>
                    
                      
                    <li class="list-group-item d-flex align-items-center">
                      <div class="ms-2 me-auto flex-grow-1">
                        <div class="fw-bold">Coondenada 1</div>
                       Introduzca el valor de la celda correspondiente a: <strong>Fila 5 Columna X</strong>
                      </div>
                      
                      <div class="col-md-1 or-0">
                          
                              <input type="text" class="form-control">
                          
                          
                       </div>
                       <span class="arrow-icon ">
                        <img src="http://localhost:3000/assets/left-arrow.png" alt="">
                      </span>
                    </li>

                    
                    <li class="list-group-item d-flex align-items-center">
                        <div class="ms-2 me-auto flex-grow-1">
                          <div class="fw-bold">Coondenada 2</div>
                         Introduzca el valor de la celda correspondiente a: <strong>Fila 5 Columna X</strong>
                        </div>
                        
                        <div class="col-md-1 or-0">
                                <input type="text" class="form-control">
                         </div>
                         <span class="arrow-icon ">
                          <img src="http://localhost:3000/assets/left-arrow.png" alt="">
                        </span>
                    </li>

                    <li class="list-group-item d-flex align-items-center">
                        <div class="ms-2 me-auto flex-grow-1">
                          <strong>Codigo de validación</strong>
                        </div>
                        
                        <div class="col-md-3 or-0">
                                <input type="text" class="form-control">
                         </div>
                         <span class="arrow-icon ">
                          <img src="http://localhost:3000/assets/left-arrow.png" alt="">
                        </span>
                    </li>
                    
                  </ol> 
               </form>
               <div class="pt-3 col-md-10 offset-md-1 text-center">
                   Haz click sobre el simbolo <img src="http://localhost:3000/assets/left-arrow.png" width="18px" alt=""> para posicionarse en el área e ingrese el dato solicitado
               </div>
               <div class="col-md-12">
                    <div class="row pt-5">
                        <div class="w-100">
                            <div class="float-left">
                                <button class="custom-primary-button">REGRESAR</button>
                            </div>

                            <div class="float-right">
                                <button class="custom-primary-button">ACEPTAR</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
           </div>
            
            
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>