<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Afiliaciones</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="heading mb-2">
                <h3>AFILIACIONES</h3>
                
            </div>
            <div class="container pt-3">
                <p class="text-justify">Ingrese los datos de la cuenta destino. Coloque el correo electrónico
                    del titular de la cuenta a afiliar para que el sistema automáticamente le envíe un mensaje
                    con detalle de la operación que usted efectuó.
                </p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center section-title">
                            <b>Afiliaciones de cuentas</b>
                        </p>
                    </div>
                    
                </div>
                <div class="row">
                    <form class="custom-form col-md-12">
                        <div class="col-md-10 offset-md-1  pt-3">
                            <div class="row text-center">

                                <div class="col-md-6">

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="customRadioInline" class="custom-control-input">
                                        <label class="custom-control-label red" for="customRadioInline1">
                                            A TERCEROS BANCO <br> DEL TESORO
                                        </label>
                                    </div>

                                    
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="customRadioInline" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">
                                            A TERCEROS <br>OTROS BANCOS
                                        </label>
                                    </div>
                                </div>
                                
                                
                            </div>
                            
                            <form class="custom-form">
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            
                                            <input type="text" class="form-control" placeholder="Número de cuenta">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            
                                            <input type="text" class="form-control" placeholder="Nombre del beneficiario">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="row">
                                            <div class="select-group col-md-3 required mt-0">
                                        
                                                <select class="text-center">
                                                  <option selected>V</option>
                                                  <option>...</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-md-9">
                                                <div class="form-group required">
                                                    
                                                    <input type="text" class="form-control" placeholder="Cédula del beneficiario">
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-7">
                                        
                                            <div class="form-group">
                                                
                                                <input type="text" class="form-control" placeholder="Correo Electrónico del Beneficiario">
                                            </div>
                                        
                                    </div>
                                </div>

                                <div class="row pt-3">
                                    <div class="col-md-7">
                                        
                                        <div class="form-group">
                                            
                                            <input type="text" class="form-control" placeholder="Nombre o Alias">
                                        </div>
                                       
                                    </div>
                                </div>
                               
                            </form>
                        </div>
                        <div class="col-md-12">
                           
                                <span>
                                    * Campos obligatorios
                                </span>
                                
                           
                        </div>
                        
                        <div class="col-md-10 offset-md-1">
                            <div class="row pt-5">
                                <div class="col-md-12">
                                    <div class="float-left">
                                        <button class="custom-primary-button">REGRESAR</button>
                                    </div>
        
                                    <div class="float-right">
                                        <button class="custom-primary-button">ACEPTAR</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </form>
                </div>
                
            </div>
            
            
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>