<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Tarjeta de alimentación</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
           
            <div class="container">
                <div class="row">
                    <div class="heading ">
                        <h3>TRANSFERENCIAS INMEDIATAS</h3>
                        
                    </div>
                </div>
                
                <p class="text-justify py-2">Esta opción te permite transferir de tus cuentas a las de terceros que sean
                    acreditadas de forma inmediata. Para registrar las cuentas destino dirígete a
                    la opción <strong>"Afiliaciones"</strong>  del menú Transferencias. Hecho
                    esto podrás generar el tipo de pago.
                </p>

                <div class="suboption-odd  pt-1 pb-2" id="horizontal_menu">
                    <div class="d-flex align-items-center justify-content-around">
                        <div class="text-center">
                            <a href="http://localhost:3000/pages/transferencias/transferencias_inmediatas/transferencias_inmediatas.jsp" class="false">
                                <b>TRANSFERENCIAS <br> INMEDIATAS</b> 
                             </a>
                        </div>
                        <div class="text-center ">
                            <a href="http://localhost:3000/pages/transferencias/transferencias_inmediatas/pago_inmediato.jsp" class="red">
                                <b>PAGO INMEDIATO</b> 
                             </a>
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="container pt-3">
                <div class="table-responsive">
                    <form action="">
                        <table class="table custom-table">
                            
                            <thead class="blue-head">
                                <th>Desde</th>
                                <th>Description</th>
                                <th>Número</th>
                                <th>Saldo Disponible</th>
                                <th>Expresado en petro</th>
                                <th>Moneda</th>
                                <th>Nombre o alias</th>
                            </thead>
                            
                            <tbody class="text-center">
                              <tr>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
                                      </div>
        
                                </td>
                                <td>Cuenta electronica</td>
                                <td>55555</td>
                                <td>555,55</td>
                                <td>555,55</td>
                                <td>VES</td>
                                <td>nada</td>
                              </tr>
                              
                            </tbody>
                          </table>
        
                    </form>
                </div>
                
            </div>
            <div class="container">
                <div class="row">
                    <form class="custom-form col-md-12">
                        <div class="col-md-10 offset-md-1  pt-3">
                            <div class="row pb-3">
                                <div class="col-md-12 pb-3">
                                    <label>Enviar pago inmediato a</label>
                                </div>
                                <div class="col-md-6">

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="customRadioInline" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1"> Teléfono</label>
                                    </div>

                                    
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="customRadioInline" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Cuenta</label>
                                    </div>
                                </div>
                            </div>   
                            
                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <label>Información de destino del pago</label>
                                </div>
                            </div>
                                
                    
                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <div class="select-group required">
                                        <span>Cuenta</span>
                                        <select>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <span for="">Monto</span>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>

                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <span for="">Descripción</span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                              
                        </div>
                        <div class="row pt-3">
                            <span>
                                * Campos obligatorios
                            </span>
                            
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-12 text-center">
                                <button class="custom-primary-button">ACEPTAR</button>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
            
            
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>