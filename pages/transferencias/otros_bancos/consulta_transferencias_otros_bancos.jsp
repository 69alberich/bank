<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Tarjeta de alimentación</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="container">
                <div class="row">
                    <div class="heading mb-2 ">
                        <h3>OTROS BANCOS</h3>
                    </div>
                    <div class="suboption-odd  pt-1 pb-2 w-100" id="horizontal_menu">
                        <div class="d-flex align-items-center justify-content-around">
                            <div class="submenu-item text-center">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/otros_bancos.jsp" class="false">
                                    <b>OTROS BANCOS</b> 
                                 </a>
                            </div>
                            <div class="submenu-item text-center ">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="red">
                                    <b>CONSULTA TRANSFERENCIA <br> OTROS BANCOS</b> 
                                 </a>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="container">
                <form class="custom-form">
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <div class="select-group">
                            
                                <select>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        
                        
                    </div>
            
                    <div class="row pt-3">
                        <div class="col-md-6">
                            <label for="">Desde</label>
                            <div class="form-group">
                                <input type="date" class="form-control" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Hasta</label>
                            <div class="form-group">
                                <input type="date" class="form-control" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md 12">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="customRadioInline" class="custom-control-input">
                                <label class="custom-control-label bold-blue" for="customRadioInline1"> Todo el año</label>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row pt-3">
                        <div class="col-md-12 text-center">
                            <button class="custom-primary-button">ACEPTAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>