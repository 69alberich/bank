<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | SUMAT</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
           
            <div class="container">
                <div class="row">
                    <div class="heading ">
                        <h3>AUTOLIQUIDACIÓN SUMAT</h3>
                        
                    </div>
                </div>
                
                <p class="text-justify py-2">Esta opción te permite realizar los pagos de impuestos
                    de la Superintendencia Municipal de Administración Tributaria (SUMAT).
                    ingrese correctamente los datos que se encuentran en la plantilla suministrada.
                </p>
            </div>
            
            <div class="container pt-3">
                <div class="table-responsive">
                    <form action="">
                        <table class="table custom-table">
                            
                            <thead class="blue-head">
                                <th>Selección</th>
                                <th>Description</th>
                                <th>Número</th>
                                <th>Saldo Disponible</th>
                                <th>Nombre o alias</th>
                            </thead>
                            
                            <tbody class="text-center">
                              <tr>
                                <td>
                                    <div class="form-check pl-1">
                                        <input class="custom-control-input " type="radio" name=""  value="option1">
                                        
                                      </div>
        
                                </td>
                                <td>Cuenta corriente</td>
                                <td>XXXX-0707-XX-XXXXXX0246</td>
                                <td>0,00</td>
                                <td>Alias no definido</td>
                                
                              </tr>

                              <tr>
                                <td>
                                    <div class="form-check pl-1">
                                        <input class="custom-control-input " type="radio" name=""  value="option1">
                                        
                                      </div>
        
                                </td>
                                <td>Cuenta corriente</td>
                                <td>XXXX-0707-XX-XXXXXX0246</td>
                                <td>0,00</td>
                                <td>Alias no definido</td>
                                
                              </tr>
                              
                            </tbody>
                          </table>
        
                    </form>
                </div>
                
            </div>
            <div class="container">
                <div class="row">
                    <form class="custom-form col-md-12">
                        <div class="col-md-10 offset-md-1  pt-3">

                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="row">
                                        <span  class="col-sm-6 col-form-label mr-0">Numero de Planilla</span>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control"  >
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <span  class="col-sm-6 col-form-label">Monto a cancelar</span>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control"  >
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="row pt-3">
                                <div class="col-md-6">
                                    <div class="row">
                                        <span class="col-sm-6 col-form-label">Tipo de atributo</span>
                                        <div class="col-sm-6">
                                            <select class="select-type-2">
                                                <option>Seleccione...</option>
                                            </select>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <span  class="col-sm-6 col-form-label">Parroquia</span>
                                        <div class="col-sm-6">
                                            
                                                
                                                <select class="select-type-2">
                                                    <option>Seleccione...</option>
                                                </select>
                                            
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-12 pt-3">
                                    <span>Período de pago</span>
                                </div>
                                
                            </div>
                            
                            <div class="row pt-3">
                                <div class="col-md-6">
                                    <div class="row">
                                        <span  class="col-sm-6 col-form-label">Desde</span>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" >
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <span  class="col-sm-6 col-form-label mr-1">Hasta</span>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control">
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-12 pt-3">
                                    <span>Estimado cliente le recordamos que los datos requeridos deben coincidir con 
                                        los que se encuentran en la planilla
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                    
                        <div class="row pt-3">
                            <div class="col-md-12 text-center">
                                <button class="custom-primary-button">CONTINUAR</button>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
            
            
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>