<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | SENIAT</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="row">
                <div class="heading mb-2">
                    <h3>PAGO DE IMPUESTOS SENIAT</h3>
                    
                </div>
            </div>
            
            <div class="container pt-5">
                <p class="text-justify">Está seguro que desea pagar <strong>Bs. 3200,00</strong>
                    por concepto de <strong>DECLARACIÓN I.S.L.R</strong> Vía internet personas naturales
                    debitado de la cuenta xxxxxxxxxx-0163
                </p>
            </div>
            <div class="container">
                
                <div class="row">
                    <form class="custom-form col-md-12">
                        
                        <div class="row pt-5">
                            <div class="col-md-12">
                                <div class="float-left">
                                    <button class="custom-primary-button">CANCELAR</button>
                                </div>
    
                                <div class="float-right">
                                    <button class="custom-primary-button">ACEPTAR</button>
                                </div>
                            </div>
                            
                        </div>
                       
                        
                    </form>
                </div>
                
            </div>
            
            
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>