<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | SAREN</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <div class="container">
                <div class="row">
                    <div class="heading mb-2">
                        <h3>RESULTADO DE LA OPERACIÓN PAGO SENIAT</h3>
                        
                    </div>
                </div>
                
            </div>
            
            <div class="container pt-3">
                
                <p class="text-justify">
                    <img class="img-fluid" src="http://localhost:3000/assets/seniat-logo.png" alt="">
                     <strong class="mx-3">11-01-2022</strong> <strong>2:10:49</strong> 
                   
                </p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="heading-blue mb-2">
                        <h3 class="text-center">PAGO SENIAT</h3>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="container">
                        <form action="" class="custom-form">
                         <ol class="list-group">
                             <li class="list-group-item d-flex justify-content-between align-items-start">
                                <div class="ms-2 me-auto">
                                    <strong>Número de plantilla</strong>
                                  </div>
                                  <span class=""><strong>221000000034</strong></span>
                               
                             </li>
                             
                              <li class="list-group-item d-flex justify-content-between align-items-start">
                                <div class="ms-2 me-auto">
                                    <strong>Referencia</strong>
                                  </div>
                                  <span class=""><strong>221000000034</strong></span>
                                
                              </li>

                              <li class="list-group-item d-flex justify-content-between align-items-start">
                                <div class="ms-2 me-auto">
                                    <strong>Cuenta de débito</strong>
                                  </div>
                                  <span class=""><strong>221000000034</strong></span>
                                
                              </li>

                              <li class="list-group-item d-flex justify-content-between align-items-start">
                                <div class="ms-2 me-auto">
                                    <strong>Concepto</strong>
                                  </div>
                                  <span class=""><strong>221000000034</strong></span>
                                
                              </li>
                              
                              <li class="list-group-item d-flex justify-content-between align-items-start">
                                <div class="ms-2 me-auto">
                                    <strong>Monto</strong>
                                  </div>
                                  <span class=""><strong>221000000034</strong></span>
                                
                              </li>
                             
                           </ol> 
                        </form>
                        
                        <div class="col-md-10 offset-md-1">
                         <div class="row pt-5">
                             <div class="col-md-12 text-center py-3">
                                 <strong>Si desea ver sus saldos actualizados, <span class="red">presiona aquí</span></strong>
                             </div>
                             <div class="col-md-12">
                                 <div class="text-center">
                                     <button class="custom-primary-button">IMPRIMIR</button>
                                 </div>
             
                                
                             </div>
                             
                         </div>
                     </div>
                    </div>
                </div>
                
                
            </div>
            
            
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>