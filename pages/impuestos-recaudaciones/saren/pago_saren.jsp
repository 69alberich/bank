<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Saren</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            <form class="custom-form col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="heading ">
                            <h3>PAGO DE SAREN</h3>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-justify pt-3 mb-1">
                                <strong>Pagar Planilla Saren</strong>
                            </p>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="text"  class="form-control">
                        </div>
                      </div>
                    
                </div>

                <div class="container">
                    
                    <div class="row pt-3">
                        <div class="table-responsive">
                            <form action="">
                                <table class="table custom-table">
                                    
                                    <thead class="blue-head">
                                        <th colspan="2">Datos del beneficiario</th>
                                        
                                    </thead>
                                    
                                    <tbody class="text-center">
                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Tarjeta afiliada
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>
                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Acto
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>

                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Nombre y Apellido
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>

                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Documento de Identidad
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>
                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Oficina
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>

                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Fecha Emisión
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>

                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Monto a Cancelar en Bs.
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>

                                      <tr>
                                        <td style="width: 20%;" class="text-right">
                                            Cuenta a Debitar
                
                                        </td>
                                        <td class="text-justify">9999-9999-9999-9999</td>
                                        
                                      </tr>
                                    </tbody>
                                  </table>
                
                            </form>
                        </div>
                        
                    </div>
                </div>

                <div class="col-md-10 offset-md-1">
                    <div class="row pt-5">
                        <div class="col-md-12">
                            <div class="float-left">
                                <button class="custom-primary-button">REGRESAR</button>
                            </div>

                            <div class="float-right">
                                <button class="custom-primary-button">PAGAR</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
              
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>