<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Afiliaciones</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            
            <div class="container ">
                <div class="row">
                    <div class="heading mb-3 pb-1">
                        <h3>TARJETA CONFIDENCIAL</h3>
                        
                    </div>
                    
                    <div class="suboption-odd  pt-1 pb-2" id="horizontal_menu">
                        <div class="d-flex align-items-center justify-content-around">
                            <div class="text-center">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/otros_bancos.jsp" class="false">
                                    <b>SOLICITUD</b> 
                                 </a>
                            </div>
                            <div class="text-center ">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="red">
                                    <b>ACTIVACION</b> 
                                 </a>
                            </div>
                            <div class="text-center ">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="false">
                                    <b>ANULACIÓN</b> 
                                 </a>
                            </div>
                        </div>
                    </div>

                    
                </div>
                <div class="row pt-3">
                    <div class="heading-blue mb-2">
                        <h3>ACTIVACIÓN DE TARJETA CONFIDENCIAL</h3>
                        
                    </div>

                    <p>Recuerde que es necesario que tenga a la mano sus productos como: chequera, tarjeta de débito,
                        libreta de ahorro para que pueda completar satisfactoriamente la activación de su Tarjeta
                        Confidencial
                    </p>
                </div>

                <div class="row">
                    <div class="col-md-6 offset-md-3">

                        <form class="custom-form col-md-12">
                            <div class="col-md-10 offset-md-1  pt-3">
                                
                                <form class="custom-form">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="select-group col-md-3 col-sm-12 pt-3">
                                            
                                                    <select class="text-center">
                                                      <option selected>V</option>
                                                      <option>...</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-9 col-sm-12 pt-3">
                                                    <div class="form-group ">
                                                        
                                                        <input type="text" class="form-control" placeholder="Cédula">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>

                                   
                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span for="">Tipo de cuenta</span>

                                                <div class="select-group mt-0">
                                        
                                                    <select class="text-center">
                                                        <option selected>Corriente No Remunerada</option>
                                                        <option>...</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                 

                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span for="">Número de cuenta</span>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group ">
                                                        <span for="">Codigo Seguridad</span>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-5 text-center">
                                                    <br>
                                                    <button id="actualizar-button" class="custom-secondary-button right-arrow">ACTUALIZAR 
                                                        <img src="http://localhost:3000/assets/right-arrow.png" alt="">
                                                    </button>
                                                    
                                                </div>
                                            </div>
                                            <div class="row pt-3">
                                                <label class="text-center">
                                                    <a href="#">
                                                    (Precione el botón "Actualizar", si requiere que
                                                            el Código de Seguridad sea reemplazado por otro.)
                                                    </a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                           
                                                <div class="form-group ">
                                                    <span for="">Escriba el código que se muestra arriba</span>
                                                    <input type="text" class="form-control">
                                                </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                           
                                                <div class="form-group ">
                                                    <span for="">Número de Tarjeta de Débito</span>
                                                    <input type="text" class="form-control">
                                                </div>
                                            
                                        </div>
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-5">
                                           
                                                <div class="form-group ">
                                                    <span for="">CVC2</span>
                                                    <input type="text" class="form-control">
                                                </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-7 ">
                                            <div class="row pt-3">
                                                <span for="" class="">Fecha de expiración</span>
                                                <div class="select-group col-md-6 ">
                                            
                                                    <select class="text-center">
                                                      <option selected>MES</option>
                                                      <option>...</option>
                                                    </select>
                                                </div>
                                                <div class="select-group col-md-6 pt-xs-3">
                                                
                                                    <select class="text-center">
                                                      <option selected>AÑO</option>
                                                      <option>...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
    

                                   
                                </form>
                            </div>
                            
                            <div class="col-md-10 offset-md-1">
                                <div class="row pt-5">
                                    <div class="col-md-12">
                                        <div class="float-start">
                                            <button class="custom-primary-button">REGRESAR</button>
                                        </div>
            
                                        <div class="float-end">
                                            <button class="custom-primary-button">ACEPTAR</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>