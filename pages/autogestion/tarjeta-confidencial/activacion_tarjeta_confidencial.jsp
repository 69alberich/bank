<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Afiliaciones</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            
            <div class="container ">
                <div class="row">
                    <div class="heading mb-3 pb-1">
                        <h3>TARJETA CONFIDENCIAL</h3>
                        
                    </div>
                    
                    <div class="suboption-odd  pt-1 pb-2" id="horizontal_menu">
                        <div class="d-flex align-items-center justify-content-around">
                            <div class="text-center">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/otros_bancos.jsp" class="false">
                                    <b>SOLICITUD</b> 
                                 </a>
                            </div>
                            <div class="text-center ">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="red">
                                    <b>ACTIVACION</b> 
                                 </a>
                            </div>
                            <div class="text-center ">
                                <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="false">
                                    <b>ANULACIÓN</b> 
                                 </a>
                            </div>
                        </div>
                    </div>

                    
                </div>
                <div class="row pt-3">
                    <div class="heading-blue mb-2">
                        <h3>ACTIVACIÓN DE TARJETA CONFIDENCIAL</h3>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="container">

                        <form class="custom-form col-md-12">
                            <div class="col-md-8 offset-md-2  pt-3">
                                
                                <div class="card bg-light mb-3 custom-card-blue">
                                    
                                    <div class="card-body text-center pt-5 pb-5">
                                      <h5 class="card-title">Su Tarjeta Confidencial ha sido activada satisfactoriamente</h5>
                                      
                                    </div>
                                  </div>
                            </div>
                            <div class="row pt-4 text-center">
                                <p>Si desea ver los datos actualizados <a href="#"><strong>PRESIONE AQUÍ</strong></a></p>
                            </div>
                            <div class="col-md-10 offset-md-1">
                                <div class="row pt-5">
                                    <div class="col-md-12">
                                        <div class="float-start">
                                            <button class="custom-primary-button">CONTINUAR</button>
                                        </div>
            
                                        <div class="float-end">
                                            <button class="custom-primary-button">IMPRIMIR</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>