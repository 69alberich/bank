<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | Afiliaciones</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

        <div class="resumen">
            
            <div class="container ">
                <div class="row">
                    <div class="heading mb-3 pb-1">
                        <h3>SOLICITUD DE REFERENCIAS</h3>
                        
                    </div>
                    
                </div>
                

                <div class="row">
                    <div class="container">

                        <form class="custom-form col-md-12">
                           <div class="row pt-5">
                               
                               
                               <a href="#" class="text-center">
                                <img src="http://localhost:3000/assets/pdf.png" width="50">
                                <br>
                                   <strong>CONSULTE AQUI SU <br> REFERENCIA BANCARIA</strong>
                                </a>
                           </div> 
                            
                            <div class="row pt-5 text-center">
                                <p>Para visualizar esta información debe tener instalado Acrobat Reader,
                                    sí no lo tiene descarguelo
                                    <a href="#">
                                        <strong>AQUÍ</strong>
                                    </a>
                                </p>
                            </div>
                            
                            <div class="row pt-5 text-center">
                                <p>Click aquí Para descargar su 
                                    <a href="#">
                                        <strong>REFERENCIA BANCARIA</strong>
                                    </a>
                                </p>
                            </div>

                            <div class="col-md-10 offset-md-1">
                                <div class="row pt-5">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <button class="custom-primary-button">REGRESAR</button>
                                        </div>
            
                                     
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>