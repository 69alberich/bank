<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Banco del tesoro | SMS - BT</title>

    <link rel="stylesheet" href="http://localhost:3000/css/resumen.css">
    <link rel="stylesheet" href="http://localhost:3000/css/footer.css">
    <link rel="stylesheet" href="http://localhost:3000/css/header.css">
    <link rel="stylesheet" href="http://localhost:3000/css/commons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="header"></div>

    <div class="layout">

            
            <div class="resumen">
                <div class="container">
                    <div class="row">
                        <div class="heading mb-2">
                            <h3>SMS BT</h3>
                        </div>
                        <div class="suboption-odd  pt-1 pb-2 w-100" id="horizontal_menu">
                            <div class="d-flex align-items-center justify-content-around ">
                                <div class="text-center">
                                    <a href="http://localhost:3000/pages/transferencias/otros_bancos/otros_bancos.jsp" class="red">
                                        <b>INFORMACIÓN</b> 
                                     </a>
                                </div>
                                <div class="text-center ">
                                    <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="flase">
                                        <b>OPCIONES <br> DEL SERVICIO</b> 
                                     </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="row pt-2">
                        <div class="suboption-odd w-100 pt-1 pb-2 overflow-auto">
                            <div class="d-flex align-items-center">
                                <div class="submenu-item text-center">
                                    <a class="text-center" href="http://localhost:3000/pages/transferencias/otros_bancos/otros_bancos.jsp" class="red">
                                        <b>AFILIACIÓN</b> 
                                     </a>
                                </div>
                                <div class="submenu-item text-center ">
                                    <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="flase">
                                        <b>ACTUALIZACIONES <br> DE AFILIACIÓN</b> 
                                     </a>
                                </div>
                                <div class="submenu-item text-center ">
                                    <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="flase">
                                        <b>ACTUALIZACIONES <br> DE TELÉFONO</b> 
                                     </a>
                                </div>
                                <div class="submenu-item text-center ">
                                    <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="flase">
                                        <b>REACTIVAR <br> SERVICIO</b> 
                                     </a>
                                </div>
                                <div class=" submenu-item text-center ">
                                    <a href="http://localhost:3000/pages/transferencias/otros_bancos/consulta_transferencias_otros_bancos.jsp" class="flase">
                                        <b>DESACTIVAR <br> SERVICIO</b> 
                                     </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="heading-blue mb-2 w-100">
                            <h3>AFILIACIÓN AL SERVICIO DE MENSAJERÍA TELEFÓNICA</h3>
                            
                        </div>
                    </div>

                    

                    <div class="row">
                        <div class="col-md-10 offset-md-1 col-sm-12 offset-sm-0">
                            <form class="custom-form row">
                                <div class="col-md-10 offset-md-1  pt-3">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="select-group">
                                                <span>Cuenta para el cobro del servicio</span>
                                                <select>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                            <div class="select-group">
                                                <span>Sobre las operaciones a esta cuenta se generarán los mensajes SMS</span>
                                                <select>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-6">
                                            <div class="select-group">
                                                <span>Seleccione su operadora telefónica</span>
                                                <select>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-8">
                                            <span>Introduzca el número telefónico que desea afiliar al servicio</span>
                                            <div class="row align-items-center">
                                                <div class="select-group col-md-6">
                                            
                                                    <select class="text-center">
                                                      <option selected>Codigo de área</option>
                                                      <option>...</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-6 pt-sm-3 pt-xs-3">
                                                    <div class="form-group">
                                                        
                                                        <input type="text" class="form-control" placeholder="Número telefónico">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>

                                    <div class="row pt-3">
                                        <div class="col-md-12 pb-3">
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="customRadioInline" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline1">Transferencias</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">

                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline2" name="customRadioInline" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline2"> ATM/POS</label>
                                            </div>
                                           
                                        </div>
                                        <div class="col-md-4">

                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline3" name="customRadioInline" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline3"> ABONOS DE CUENTA</label>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    
                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                            <div class="row text-center">
                                                <div class="col-md-6">
                                                    
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="customRadioInline4" name="customRadioInline" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadioInline4"> SELECCIONAR TODAS</label>
                                                    </div>

                                                   
                                                </div>
                                                
                                                <div class="col-md-6">

                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="customRadioInline5" name="customRadioInline" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadioInline5"> DESELECCIONAR TODAS</label>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                      
                                    </div> 
                                </div>
                             
                                <div class="col-md-10 offset-md-1">
                                    <div class="row pt-5">
                                        <div class="col-md-12">
                                            <div class="float-left">
                                                <button class="custom-primary-button">CANCELAR</button>
                                            </div>
                
                                            <div class="float-right">
                                                <button class="custom-primary-button">ACEPTAR</button>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        
    </div>

    <div class="footer"></div>
</body>
<script>
    $(document).ready(function () {
        $('.header').load('http://localhost:3000/components/header.html');
        $('.footer').load('http://localhost:3000/components/footer.html');
        
    });
    
</script>

</html>